package com.devcamp.task56b10.Controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task56b10.models.Circle;
import com.devcamp.task56b10.models.Cylinder;

@RestController
public class CircleCylinderAPI {
    @GetMapping("/circle-area")
    public double getAreaCircle(@RequestParam double radius){
        Circle circle = new Circle(radius);
        return circle.getArea();
    }

    @GetMapping("/cylinder-volume")
    public double getCylinderVolume(@RequestParam double radius,
            @RequestParam double height) {
        Cylinder cylinder = new Cylinder(radius, height);
        return cylinder.getVolume();
    }

}
