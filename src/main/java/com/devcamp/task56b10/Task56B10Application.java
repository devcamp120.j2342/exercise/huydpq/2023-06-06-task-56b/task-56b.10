package com.devcamp.task56b10;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task56B10Application {

	public static void main(String[] args) {
		SpringApplication.run(Task56B10Application.class, args);
	}

}
